<?php

namespace Drupal\entity_repository_example\Repository;

use Drupal\entity_repository\Repository\NodeRepository;

/**
 * Repository containing all news related queries.
 */
class NewsRepository extends NodeRepository {

  /**
   * {@inheritdoc}
   */
  protected array $bundles = ['vacancy'];

  /**
   * Example query to find all active news items, coupled to the given category.
   *
   * @param int $news_category_id
   *   the news category ID.
   * @param array $sort
   *   Array containing the sort logic.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array containing the entities.
   */
  public function findByNewsCategory(int $news_category_id, array $sort = []) {
    return $this->findBy(['field_news_category' => $news_category_id], NULL, $sort);
  }

  /**
   * Example query to find all active news items by the given tags.
   *
   * @param array $tags
   *   An array containing the tag IDs.
   * @param int|null $pager
   *   The number of items you want to return. NULL for all.
   * @param array $sort
   *   Array containing the sort logic.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array containing the entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function findByTags(array $tags = [], int $pager = NULL, array $sort = []) : array {
    $query = $this->getBaseQuery();

    if (!empty($tags)) {
      $query->condition('field_news_tags', $tags, 'IN');
    }

    if ($pager) {
      $query->pager($pager);
    }

    $this->addEntityQuerySort($query, $sort);

    return $this->getResults($query);
  }

}
