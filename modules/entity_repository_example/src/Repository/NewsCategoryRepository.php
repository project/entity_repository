<?php

namespace Drupal\entity_repository_example\Repository;

use Drupal\entity_repository\Repository\TaxonomyTermRepository;
use Drupal\node\NodeInterface;

/**
 * Repository containing all news category related queries.
 */
class NewsCategoryRepository extends TaxonomyTermRepository {

  /**
   * {@inheritdoc}
   */
  protected array $vocabularies = ['news_category'];

  /**
   * An example query.
   *
   * Query returning all "news category" taxonomy terms that are coupled to an
   * existing and published news item.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array containing the entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function findAllCoupledToNewsItem(): array {
    $select = $this->connection->select('taxonomy_term_field_data', 't');
    $select->fields('t', ['tid', 'weight']);
    $select->distinct();

    $select->innerJoin('node__field_news_category', 'nc', 't.tid = nc.field_news_category_target_id AND nc.deleted = 0');
    $select->innerJoin('node_field_data', 'n', 'n.nid = e.entity_id');

    $select->condition('t.langcode', $this->getLangCode());
    $select->condition('t.vid', $this->getVocabularies(), 'IN');
    $select->condition('n.status', NodeInterface::PUBLISHED);

    $select->orderBy('t.weight', 'asc');

    $statement = $select->execute();

    if ($statement === NULL) {
      return [];
    }

    $results = $statement->fetchCol();

    if (empty($results)) {
      return [];
    }

    return $this->entityTypeManager
      ->getStorage($this->entityType)
      ->loadMultiple($results);
  }

}
