<?php

namespace Drupal\entity_repository_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_repository_example\Repository\NewsCategoryRepository;
use Drupal\entity_repository_example\Repository\NewsRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller containing entity repository examples.
 */
class EntityRepositoryExampleController extends ControllerBase {

  /**
   * The node view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $nodeViewBuilder;

  /**
   * The news repository.
   *
   * @var \Drupal\entity_repository_example\Repository\NewsRepository
   */
  protected $newsRepository;

  /**
   * The news category repository.
   *
   * @var \Drupal\entity_repository_example\Repository\NewsCategoryRepository
   */
  protected $newsCategoryRepository;

  /**
   * EntityRepositoryExampleController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\entity_repository_example\Repository\NewsRepository $newsRepository
   *   The news repository.
   * @param \Drupal\entity_repository_example\Repository\NewsCategoryRepository $newsCategoryRepository
   *   The news category repository.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, NewsRepository $newsRepository, NewsCategoryRepository $newsCategoryRepository) {
    $this->newsRepository = $newsRepository;
    $this->newsCategoryRepository = $newsCategoryRepository;
    $this->nodeViewBuilder = $entityTypeManager->getViewBuilder('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_repository_example.repository.news'),
      $container->get('entity_repository_example.repository.news_category')
    );
  }

  /**
   * Example of usage of the entity repository module.
   *
   * @return array
   *   A renderable array containing a list of news items.
   */
  public function newsAction(): array {
    $news_category_id = 1;
    $news_items = $this->newsRepository->findByNewsCategory($news_category_id);

    $items = [];
    if (!empty($news_items)) {
      foreach ($news_items as $news_item) {
        $items[] = $this->nodeViewBuilder->view($news_item, 'teaser');
      }
    }

    return [
      '#theme' => 'item_list',
      '#items' => $items,
      '#empty' => $this->t('No news items found.'),
    ];
  }

}
