<?php

namespace Drupal\entity_repository_test\Repository;

use Drupal\entity_repository\Repository\TaxonomyTermRepository;

/**
 * Class containing news_category queries.
 */
class NewsCategoryRepository extends TaxonomyTermRepository {

  /**
   * {@inheritdoc}
   */
  protected array $vocabularies = ['news_category'];

}
