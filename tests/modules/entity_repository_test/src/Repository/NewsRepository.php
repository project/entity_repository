<?php

namespace Drupal\entity_repository_test\Repository;

use Drupal\entity_repository\Repository\NodeRepository;

/**
 * Class containing news queries.
 */
class NewsRepository extends NodeRepository {

  /**
   * {@inheritdoc}
   */
  protected array $bundles = ['news'];

}
