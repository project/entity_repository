<?php

namespace Drupal\entity_repository_test\Repository;

use Drupal\entity_repository\Repository\CommentRepository;

/**
 * Class containing news comment queries.
 */
class NewsCommentRepository extends CommentRepository {

  /**
   * {@inheritdoc}
   */
  protected array $bundles = ['news_comment'];

}
