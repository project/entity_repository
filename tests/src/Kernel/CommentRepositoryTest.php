<?php

namespace Drupal\Tests\entity_repository\Kernel;

use Drupal\comment\CommentInterface;
use Drupal\comment\CommentTypeInterface;
use Drupal\comment\Entity\Comment;
use Drupal\comment\Entity\CommentType;
use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\comment\Tests\CommentTestTrait;
use Drupal\entity_repository\Repository\EntityRepositoryInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Test class that contains tests for the CommentRepository class.
 *
 * @group entity_repository
 *
 * @coversDefaultClass \Drupal\entity_repository\Repository\CommentRepository
 */
class CommentRepositoryTest extends KernelTestBase {

  use NodeCreationTrait;
  use ContentTypeCreationTrait;
  use CommentTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_repository',
    'entity_repository_test',
    'comment',
    'field',
    'filter',
    'node',
    'user',
    'system',
    'text',
  ];

  /**
   * The news comment repository service.
   *
   * @var \Drupal\entity_repository_test\Repository\NewsCommentRepository
   */
  protected $newsCommentRepository;

  /**
   * A node entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node1;

  /**
   * A node entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node2;

  /**
   * A node entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node3;

  /**
   * A node entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node4;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', 'sequences');
    $this->installSchema('node', 'node_access');
    $this->installSchema('comment', 'comment_entity_statistics');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('comment');
    $this->installEntitySchema('date_format');
    $this->installConfig(['comment', 'filter', 'node']);

    // Create a node type.
    $this->createContentType([
      'type' => 'news',
      'name' => 'News',
    ]);

    // Create a node type.
    $this->createContentType([
      'type' => 'page',
      'name' => 'Page',
    ]);

    $this->createCommentType('news_comment');
    $this->createCommentType('comment');

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_test',
      'entity_type' => 'comment',
      'type' => 'string',
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $field_config = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'news_comment',
    ]);
    $field_config->save();

    $this->addDefaultCommentField('node', 'news', 'comment', CommentItemInterface::OPEN, 'news_comment');

    $this->node1 = $this->createNode(['type' => 'news']);
    $this->node2 = $this->createNode(['type' => 'news']);
    $this->node3 = $this->createNode(['type' => 'news']);
    $this->node4 = $this->createNode(['type' => 'page']);
    $this->createComment([
      'entity_id' => $this->node1->id(),
      'comment_type' => 'news_comment',
      'field_test' => 'value1',
    ]);
    $this->createComment([
      'entity_id' => $this->node1->id(),
      'comment_type' => 'news_comment',
      'field_test' => 'value1',
    ]);
    $this->createComment([
      'entity_id' => $this->node2->id(),
      'comment_type' => 'news_comment',
      'field_test' => 'value2',
    ]);
    $this->createComment([
      'entity_id' => $this->node2->id(),
      'comment_type' => 'news_comment',
      'field_test' => 'value2',
    ]);
    $this->createComment([
      'entity_id' => $this->node3->id(),
      'comment_type' => 'news_comment',
      'field_test' => 'value3',
    ]);
    $this->createComment([
      'entity_id' => $this->node4->id(),
      'comment_type' => 'comment',
    ]);

    $this->newsCommentRepository = $this->container->get('entity_repository_test.repository.news_comment');
  }

  /**
   * Test the CommentRepository findAll() method.
   *
   * @covers ::findAll
   */
  public function testFindAll(): void {
    // Make sure that findAll returns all results.
    $results = $this->newsCommentRepository->findAll();
    $this->assertCount(5, $results);

    // Make sure all results are news_comment items.
    foreach ($results as $result) {
      $this->assertEquals('news_comment', $result->bundle());
    }

    // Check if the limit is working.
    $results = $this->newsCommentRepository->findAll(2);
    $this->assertCount(2, $results);

    // Check if sorting is working.
    // Create a new comment. Since we're sorting on cid descending, this will
    // be the first comment.
    $comment = $this->createComment([
      'entity_id' => $this->node1->id(),
      'comment_type' => 'news_comment',
    ]);
    $results = $this->newsCommentRepository->findAll(1, [
      'field' => 'cid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertSame($comment->id(), $result->id());
    $results = $this->newsCommentRepository->findAll(1, [
      'field' => 'cid',
      'dir' => EntityRepositoryInterface::SORT_ASCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertNotSame($comment->id(), $result->id());
  }

  /**
   * Test the CommentRepository findBy() method.
   *
   * @covers ::findBy
   */
  public function testFindBy(): void {
    // Check if the find by functionality is working.
    $results = $this->newsCommentRepository->findBy(['field_test' => 'value1']);
    $this->assertCount(2, $results);

    $results = $this->newsCommentRepository->findBy(['field_test' => 'value3']);
    $this->assertCount(1, $results);

    // Check if the limit is working.
    $results = $this->newsCommentRepository->findBy([], 2);
    $this->assertCount(2, $results);

    $results = $this->newsCommentRepository->findBy(['field_test' => 'value1'], 1);
    $this->assertCount(1, $results);

    // Check if sorting is working.
    $comment = $this->createComment([
      'entity_id' => $this->node1->id(),
      'comment_type' => 'news_comment',
      'field_test' => 'value3',
    ]);
    $results = $this->newsCommentRepository->findBy([], 1, [
      'field' => 'cid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertSame($comment->id(), $result->id());
    $results = $this->newsCommentRepository->findBy([], 1, [
      'field' => 'cid',
      'dir' => EntityRepositoryInterface::SORT_ASCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertNotSame($comment->id(), $result->id());

    // Check if the 3 parameters are working together.
    $results = $this->newsCommentRepository->findBy(['field_test' => 'value3'], 1, [
      'field' => 'cid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertSame($comment->id(), $result->id());

    $results = $this->newsCommentRepository->findBy(['field_test' => 'value1'], 1, [
      'field' => 'cid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $result = current($results);
    $this->assertNotSame($comment->id(), $result->id());

    $comment_value4 = $this->createComment([
      'entity_id' => $this->node1->id(),
      'comment_type' => 'news_comment',
      'field_test' => 'value4',
    ]);
    $comment_value5 = $this->createComment([
      'entity_id' => $this->node1->id(),
      'comment_type' => 'news_comment',
      'field_test' => 'value5',
    ]);

    // Test findBy with multiple values.
    $results = $this->newsCommentRepository->findBy(
      [
        'field_test' => ['value4', 'value5'],
      ],
      NULL,
      [
        'field' => 'cid',
        'dir' => EntityRepositoryInterface::SORT_ASCENDING,
      ]);
    $this->assertEquals(array_keys($results), [
      (int) $comment_value4->id(),
      (int) $comment_value5->id(),
    ]);
  }

  /**
   * Tests the CommentRepository loadByIds() method.
   *
   * @covers ::loadByIds
   */
  public function testLoadByIds(): void {
    $results = $this->newsCommentRepository->loadByIds([2, 3]);
    $this->assertSame([2, 3], array_keys($results));
  }

  /**
   * Tests the CommentRepository countBaseQuery() method.
   *
   * @covers ::countBaseQuery
   */
  public function testCountBaseQuery(): void {
    $count = $this->newsCommentRepository->countBaseQuery();
    $this->assertSame(5, $count);

    $comment = $this->createComment([
      'entity_id' => $this->node1->id(),
      'comment_type' => 'news_comment',
    ]);
    $count = $this->newsCommentRepository->countBaseQuery();
    $this->assertSame(6, $count);

    $comment->setUnpublished();
    $comment->save();
    $count = $this->newsCommentRepository->countBaseQuery();
    $this->assertSame(5, $count);

    $comment->delete();
    $count = $this->newsCommentRepository->countBaseQuery();
    $this->assertSame(5, $count);
  }

  /**
   * Tests the CommentRepository countBy() method.
   *
   * @covers ::countBy
   */
  public function testCountByQuery(): void {
    $count = $this->newsCommentRepository->countBy(['field_test' => 'value1']);
    $this->assertSame(2, $count);

    $count = $this->newsCommentRepository->countBy(['field_test' => 'value2']);
    $this->assertSame(2, $count);

    $count = $this->newsCommentRepository->countBy(['field_test' => 'value3']);
    $this->assertSame(1, $count);

    $count = $this->newsCommentRepository->countBy(['field_test' => 'value4']);
    $this->assertSame(0, $count);
  }

  /**
   * Tests the CommentRepository findByCommentedEntity() method.
   *
   * @covers ::findByCommentedEntity
   */
  public function testFindByCommentedEntity(): void {
    $results = $this->newsCommentRepository->findByCommentedEntity($this->node1->getEntityTypeId(), $this->node1->id(), 'comment');
    $this->assertCount(2, $results);

    $results = $this->newsCommentRepository->findByCommentedEntity($this->node3->getEntityTypeId(), $this->node3->id(), 'comment');
    $this->assertCount(1, $results);

    // Check that limit is working.
    $results = $this->newsCommentRepository->findByCommentedEntity($this->node1->getEntityTypeId(), $this->node1->id(), 'comment', 1);
    $this->assertCount(1, $results);

    // Check if sorting is working.
    $comment = $this->createComment([
      'entity_id' => $this->node1->id(),
      'comment_type' => 'news_comment',
      'field_test' => 'value3',
    ]);
    $results = $this->newsCommentRepository->findByCommentedEntity($this->node1->getEntityTypeId(), $this->node1->id(), 'comment', 1, [
      'field' => 'cid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertSame($comment->id(), $result->id());
    $results = $this->newsCommentRepository->findByCommentedEntity($this->node1->getEntityTypeId(), $this->node1->id(), 'comment', 1, [
      'field' => 'cid',
      'dir' => EntityRepositoryInterface::SORT_ASCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertNotSame($comment->id(), $result->id());

    // Check if the 3 parameters are working together.
    $results = $this->newsCommentRepository->findByCommentedEntity($this->node1->getEntityTypeId(), $this->node1->id(), 'comment', 1, [
      'field' => 'cid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertSame($comment->id(), $result->id());
  }

  /**
   * Tests the CommentRepository countByCommentedEntity() method.
   *
   * @covers ::countByCommentedEntity
   */
  public function testCountByCommentedEntity(): void {
    $count = $this->newsCommentRepository->countByCommentedEntity($this->node1->getEntityTypeId(), $this->node1->id(), 'comment');
    $this->assertSame(2, $count);

    $count = $this->newsCommentRepository->countByCommentedEntity($this->node3->getEntityTypeId(), $this->node3->id(), 'comment');
    $this->assertSame(1, $count);

    $comment = $this->createComment([
      'entity_id' => $this->node1->id(),
      'comment_type' => 'news_comment',
    ]);
    $count = $this->newsCommentRepository->countByCommentedEntity($this->node1->getEntityTypeId(), $this->node1->id(), 'comment');
    $this->assertSame(3, $count);

    $comment->setUnpublished();
    $comment->save();
    $count = $this->newsCommentRepository->countByCommentedEntity($this->node1->getEntityTypeId(), $this->node1->id(), 'comment');
    $this->assertSame(2, $count);

    $comment->delete();
    $count = $this->newsCommentRepository->countByCommentedEntity($this->node1->getEntityTypeId(), $this->node1->id(), 'comment');
    $this->assertSame(2, $count);
  }

  /**
   * Create a comment type.
   *
   * @param string $id
   *   The comment type ID.
   * @param string $target_entity_type_id
   *   The target entity type id.
   *
   * @return \Drupal\comment\CommentTypeInterface
   *   The created comment type.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createCommentType(string $id, string $target_entity_type_id = 'node'): CommentTypeInterface {
    $bundle = CommentType::create([
      'id' => $id,
      'label' => $id,
      'description' => '',
      'target_entity_type_id' => $target_entity_type_id,
    ]);
    $bundle->save();
    return $bundle;
  }

  /**
   * Create a comment.
   *
   * @param array $values
   *   Create a comment with the given array of values.
   *
   * @return \Drupal\comment\CommentInterface
   *   The created comment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createComment(array $values): CommentInterface {
    $comment = Comment::create($values + [
      'entity_type' => 'node',
      'field_name' => 'comment',
      'comment_type' => 'comment',
      'status' => TRUE,
    ]);
    $comment->save();
    return $comment;
  }

}
