<?php

namespace Drupal\Tests\entity_repository\Kernel;

use Drupal\Core\Language\LanguageInterface;
use Drupal\entity_repository\Repository\EntityRepositoryInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Test class that contains tests for the TaxonomyTermRepository class.
 *
 * @group entity_repository
 *
 * @coversDefaultClass \Drupal\entity_repository\Repository\TaxonomyTermRepository
 */
class TaxonomyTermRepositoryTest extends KernelTestBase {

  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_repository',
    'entity_repository_test',
    'taxonomy',
    'filter',
    'field',
    'text',
    'user',
    'system',
  ];

  /**
   * The news repository service.
   *
   * @var \Drupal\entity_repository_test\Repository\NewsCategoryRepository
   */
  protected $newsCategoryRepository;

  /**
   * The news category vocabulary.
   *
   * @var \Drupal\taxonomy\Entity\Vocabulary
   */
  protected $newsCategoryVocabulary;

  /**
   * The current language ID.
   *
   * @var string
   */
  protected $currentLangcode;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', 'sequences');
    $this->installEntitySchema('user');
    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('date_format');
    $this->installConfig('filter');
    $this->installConfig('taxonomy');

    $this->currentLangcode = $this->container->get('language_manager')
      ->getCurrentLanguage()
      ->getId();
    $this->newsCategoryVocabulary = Vocabulary::create([
      'name' => 'News Category',
      'description' => 'News category vocabulary',
      'vid' => 'news_category',
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'weight' => random_int(0, 10),
    ]);
    $this->newsCategoryVocabulary->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_test',
      'entity_type' => 'taxonomy_term',
      'type' => 'string',
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $field_config = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $this->newsCategoryVocabulary->id(),
    ]);
    $field_config->save();

    $this->createTerm($this->newsCategoryVocabulary, [
      'field_test' => 'value1',
      'langcode' => $this->currentLangcode,
    ]);
    $this->createTerm($this->newsCategoryVocabulary, [
      'field_test' => 'value1',
      'langcode' => $this->currentLangcode,
    ]);
    $this->createTerm($this->newsCategoryVocabulary, [
      'field_test' => 'value2',
      'langcode' => $this->currentLangcode,
    ]);
    $this->createTerm($this->newsCategoryVocabulary, [
      'field_test' => 'value2',
      'langcode' => $this->currentLangcode,
    ]);
    $this->createTerm($this->newsCategoryVocabulary, [
      'field_test' => 'value3',
      'langcode' => $this->currentLangcode,
    ]);

    $this->newsCategoryRepository = $this->container->get('entity_repository_test.repository.news_category');
  }

  /**
   * Test the TaxonomyTermRepository findAll() method.
   *
   * @covers ::findAll
   */
  public function testFindAll(): void {
    $unpublished_term = $this->createTerm($this->newsCategoryVocabulary, [
      'status' => FALSE,
      'langcode' => $this->currentLangcode,
    ]);

    // Make sure that findAll returns all results.
    $results = $this->newsCategoryRepository->findAll();
    $this->assertCount(5, $results);
    $this->assertArrayNotHasKey($unpublished_term->id(), $results);

    // Make sure all results are news items.
    foreach ($results as $result) {
      $this->assertEquals($this->newsCategoryVocabulary->id(), $result->bundle());
    }

    // Check if the limit is working.
    $results = $this->newsCategoryRepository->findAll(2);
    $this->assertCount(2, $results);

    // Check if sorting is working.
    // Create a new term. Since we're sorting on tid descending, this will be
    // the first term.
    $term = $this->createTerm($this->newsCategoryVocabulary, ['langcode' => $this->currentLangcode]);
    $results = $this->newsCategoryRepository->findAll(1, [
      'field' => 'tid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertSame($term->id(), $result->id());
    $results = $this->newsCategoryRepository->findAll(1, [
      'field' => 'tid',
      'dir' => EntityRepositoryInterface::SORT_ASCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertNotSame($term->id(), $result->id());
  }

  /**
   * Test the TaxonomyTermRepository findBy() method.
   *
   * @covers ::findBy
   */
  public function testFindBy(): void {
    // Check if the the find by functionality is working.
    $results = $this->newsCategoryRepository->findBy(['field_test' => 'value1']);
    $this->assertCount(2, $results);

    $results = $this->newsCategoryRepository->findBy(['field_test' => 'value3']);
    $this->assertCount(1, $results);

    // Check if the limit is working.
    $results = $this->newsCategoryRepository->findBy([], 2);
    $this->assertCount(2, $results);

    $results = $this->newsCategoryRepository->findBy(['field_test' => 'value1'], 1);
    $this->assertCount(1, $results);

    // Check if sorting is working.
    $term = $this->createTerm($this->newsCategoryVocabulary, [
      'field_test' => 'value3',
      'langcode' => $this->currentLangcode,
    ]);
    $results = $this->newsCategoryRepository->findBy([], 1, [
      'field' => 'tid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertSame($term->id(), $result->id());
    $results = $this->newsCategoryRepository->findBy([], 1, [
      'field' => 'tid',
      'dir' => EntityRepositoryInterface::SORT_ASCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertNotSame($term->id(), $result->id());

    // Check if the 3 parameters are working together.
    $results = $this->newsCategoryRepository->findBy(['field_test' => 'value3'], 1, [
      'field' => 'tid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertSame($term->id(), $result->id());

    $results = $this->newsCategoryRepository->findBy(['field_test' => 'value1'], 1, [
      'field' => 'tid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $result = current($results);
    $this->assertNotSame($term->id(), $result->id());

    $term_value4 = $this->createTerm($this->newsCategoryVocabulary, [
      'field_test' => 'value4',
      'langcode' => $this->currentLangcode,
    ]);
    $term_value5 = $this->createTerm($this->newsCategoryVocabulary, [
      'field_test' => 'value5',
      'langcode' => $this->currentLangcode,
    ]);
    // Test findBy with multiple values.
    $results = $this->newsCategoryRepository->findBy(
      [
        'field_test' => ['value4', 'value5'],
      ],
      NULL,
      [
        'field' => 'tid',
        'dir' => EntityRepositoryInterface::SORT_ASCENDING,
      ]);
    $this->assertEquals(array_keys($results), [
      (int) $term_value4->id(),
      (int) $term_value5->id(),
    ]);

  }

  /**
   * Tests the TaxonomyTermRepository loadByIds() method.
   *
   * @covers ::loadByIds
   */
  public function testLoadByIds(): void {
    $results = $this->newsCategoryRepository->loadByIds([2, 3]);
    $this->assertSame([2, 3], array_keys($results));
  }

  /**
   * Tests the TaxonomyTermRepository countBaseQuery() method.
   *
   * @covers ::countBaseQuery
   */
  public function testCountBaseQuery(): void {
    $count = $this->newsCategoryRepository->countBaseQuery();
    $this->assertSame(5, $count);

    $term = $this->createTerm($this->newsCategoryVocabulary, ['langcode' => $this->currentLangcode]);
    $count = $this->newsCategoryRepository->countBaseQuery();
    $this->assertSame(6, $count);

    $term->delete();
    $count = $this->newsCategoryRepository->countBaseQuery();
    $this->assertSame(5, $count);
  }

  /**
   * Tests the NodeRepository countBy() method.
   *
   * @covers ::countBy
   */
  public function testCountByQuery(): void {
    $count = $this->newsCategoryRepository->countBy(['field_test' => 'value1']);
    $this->assertSame(2, $count);

    $count = $this->newsCategoryRepository->countBy(['field_test' => 'value2']);
    $this->assertSame(2, $count);

    $count = $this->newsCategoryRepository->countBy(['field_test' => 'value3']);
    $this->assertSame(1, $count);

    $count = $this->newsCategoryRepository->countBy(['field_test' => 'value4']);
    $this->assertSame(0, $count);
  }

}
