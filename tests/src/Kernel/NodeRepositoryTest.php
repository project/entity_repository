<?php

namespace Drupal\Tests\entity_repository\Kernel;

use Drupal\entity_repository\Repository\EntityRepositoryInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Test class that contains tests for the nodeRepository class.
 *
 * @group entity_repository
 *
 * @coversDefaultClass \Drupal\entity_repository\Repository\NodeRepository
 */
class NodeRepositoryTest extends KernelTestBase {

  use NodeCreationTrait;
  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_repository',
    'entity_repository_test',
    'field',
    'filter',
    'node',
    'user',
    'system',
    'text',
  ];

  /**
   * The news repository service.
   *
   * @var \Drupal\entity_repository_test\Repository\NewsRepository
   */
  protected $newsRepository;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', 'sequences');
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('date_format');
    $this->installConfig('filter');
    $this->installConfig('node');

    // Create a node type.
    $this->createContentType([
      'type' => 'news',
      'name' => 'News',
    ]);

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_test',
      'entity_type' => 'node',
      'type' => 'string',
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $field_config = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'news',
    ]);
    $field_config->save();

    // Create a node type.
    $this->createContentType([
      'type' => 'page',
      'name' => 'Page',
    ]);

    $this->createNode(['type' => 'news', 'field_test' => 'value1']);
    $this->createNode(['type' => 'news', 'field_test' => 'value1']);
    $this->createNode(['type' => 'news', 'field_test' => 'value2']);
    $this->createNode(['type' => 'news', 'field_test' => 'value2']);
    $this->createNode(['type' => 'news', 'field_test' => 'value3']);

    $this->createNode(['type' => 'page']);

    $this->newsRepository = $this->container->get('entity_repository_test.repository.news');
  }

  /**
   * Test the NodeRepository findAll() method.
   *
   * @covers ::findAll
   */
  public function testFindAll(): void {
    $unpublished_node = $this->createNode([
      'type' => 'news',
      'status' => FALSE,
    ]);

    // Make sure that findAll returns all results.
    $results = $this->newsRepository->findAll();
    $this->assertCount(5, $results);
    $this->assertArrayNotHasKey($unpublished_node->id(), $results);

    // Make sure all results are news items.
    foreach ($results as $result) {
      $this->assertEquals('news', $result->bundle());
    }

    // Check if the limit is working.
    $results = $this->newsRepository->findAll(2);
    $this->assertCount(2, $results);

    // Check if sorting is working.
    // Create a new node. Since we're sorting on nid descending, this will be
    // the first node.
    $node = $this->createNode(['type' => 'news']);
    $results = $this->newsRepository->findAll(1, [
      'field' => 'nid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertSame($node->id(), $result->id());
    $results = $this->newsRepository->findAll(1, [
      'field' => 'nid',
      'dir' => EntityRepositoryInterface::SORT_ASCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertNotSame($node->id(), $result->id());
  }

  /**
   * Test the NodeRepository findBy() method.
   *
   * @covers ::findBy
   */
  public function testFindBy(): void {
    // Check if the the find by functionality is working.
    $results = $this->newsRepository->findBy(['field_test' => 'value1']);
    $this->assertCount(2, $results);

    $results = $this->newsRepository->findBy(['field_test' => 'value3']);
    $this->assertCount(1, $results);

    // Check if the limit is working.
    $results = $this->newsRepository->findBy([], 2);
    $this->assertCount(2, $results);

    $results = $this->newsRepository->findBy(['field_test' => 'value1'], 1);
    $this->assertCount(1, $results);

    // Check if sorting is working.
    $node = $this->createNode(['type' => 'news', 'field_test' => 'value3']);
    $results = $this->newsRepository->findBy([], 1, [
      'field' => 'nid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertSame($node->id(), $result->id());
    $results = $this->newsRepository->findBy([], 1, [
      'field' => 'nid',
      'dir' => EntityRepositoryInterface::SORT_ASCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertNotSame($node->id(), $result->id());

    // Check if the 3 parameters are working together.
    $results = $this->newsRepository->findBy(['field_test' => 'value3'], 1, [
      'field' => 'nid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $this->assertCount(1, $results);
    $result = current($results);
    $this->assertSame($node->id(), $result->id());

    $results = $this->newsRepository->findBy(['field_test' => 'value1'], 1, [
      'field' => 'nid',
      'dir' => EntityRepositoryInterface::SORT_DESCENDING,
    ]);
    $result = current($results);
    $this->assertNotSame($node->id(), $result->id());

    $node_value4 = $this->createNode([
      'type' => 'news',
      'field_test' => 'value4',
    ]);
    $node_value5 = $this->createNode([
      'type' => 'news',
      'field_test' => 'value4',
    ]);
    // Test findBy with multiple values.
    $results = $this->newsRepository->findBy(
      [
        'field_test' => ['value4', 'value5'],
      ],
      NULL,
      [
        'field' => 'nid',
        'dir' => EntityRepositoryInterface::SORT_ASCENDING,
      ]);
    $this->assertEquals(array_keys($results), [
      (int) $node_value4->id(),
      (int) $node_value5->id(),
    ]);
  }

  /**
   * Tests the NodeRepository loadByIds() method.
   *
   * @covers ::loadByIds
   */
  public function testLoadByIds(): void {
    $results = $this->newsRepository->loadByIds([2, 3]);
    $this->assertSame([2, 3], array_keys($results));
  }

  /**
   * Tests the NodeRepository countBaseQuery() method.
   *
   * @covers ::countBaseQuery
   */
  public function testCountBaseQuery(): void {
    $count = $this->newsRepository->countBaseQuery();
    $this->assertSame(5, $count);

    $node = $this->createNode(['type' => 'news']);
    $count = $this->newsRepository->countBaseQuery();
    $this->assertSame(6, $count);

    $node->delete();
    $count = $this->newsRepository->countBaseQuery();
    $this->assertSame(5, $count);
  }

  /**
   * Tests the NodeRepository countBy() method.
   *
   * @covers ::countBy
   */
  public function testCountByQuery(): void {
    $count = $this->newsRepository->countBy(['field_test' => 'value1']);
    $this->assertSame(2, $count);

    $count = $this->newsRepository->countBy(['field_test' => 'value2']);
    $this->assertSame(2, $count);

    $count = $this->newsRepository->countBy(['field_test' => 'value3']);
    $this->assertSame(1, $count);

    $count = $this->newsRepository->countBy(['field_test' => 'value4']);
    $this->assertSame(0, $count);
  }

}
