# Entity Repository

Provides a basic API for queries. This module won't do much by itself.
Extend the base classes and create your own repository classes.

## INTRODUCTION

e.g. when creating a repository for the news content type.

### Create a service

```YAML
news.repository.news:
  class: Drupal\news\Repository\NewsRepository
  parent: entity_repository.repository.node
```

### Create a repository class

The property `$bundles` limits the results to the given bundles. The base
NodeRepository class contains a lot of queries by default like `findAll()`.

```php
<?php

namespace Drupal\news\Repository;

use Drupal\entity_repository\Repository\NodeRepository;

/**
 * Repository containing all news related queries.
 */
class NewsRepository extends NodeRepository {

  /**
   * {@inheritdoc}
   */
  protected $bundles = ['news'];

  /**
   * {@inheritdoc}
   */
  public function findByTags(array $tags = [], int $pager = NULL, $sort = NULL) : array {
    $query = $this->getBaseQuery();

    if (!empty($tags)) {
      $query->condition('field_news_tags', $tags, 'IN');
    }

    if ($pager) {
      $query->pager($pager);
    }

    if ($sort) {
      $query->sort($sort['field'], $sort['dir'], $this->getLangCode());
    }

    return $this->getResults($query);
  }

}
```

### Use the repository class

```php
$nodes = \Drupal::service('news.repository.news')->findAll();
```

### Create a repository without a custom class

When you don't need custom queries, you can create a repository without without creating an empty repository class.
You can add a new service and call the setBundles or setVocabularies for taxonomy terms on the base class.

```YAML
news.repository.news:
  class: Drupal\news\Repository\NewsRepository
  parent: entity_repository.repository.node
  calls:
    - [setBundles, [['news']]]
```

## REQUIREMENTS
None

## INSTALLATION
Install the "Entity Repository" module as
you would normally install a contributed Drupal module.

Visit https://www.drupal.org/node/1897420 for further
information.

## CONFIGURATION
This is a developer module so no configuration is needed.
