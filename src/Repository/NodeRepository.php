<?php

namespace Drupal\entity_repository\Repository;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\node\Entity\Node;

/**
 * Base class for node related queries.
 */
class NodeRepository extends EntityRepository {

  /**
   * {@inheritdoc}
   */
  protected $entityType = 'node';

  /**
   * {@inheritdoc}
   */
  protected function getBaseQuery() : QueryInterface {
    $query = $this->entityTypeManager->getStorage($this->entityType)
      ->getQuery();
    $query->condition('status', Node::PUBLISHED);
    $query->condition('type', $this->bundles, 'IN');
    $query->condition('langcode', $this->getLangCode());
    $query->addTag('node_access');
    // Add langcode metadata. This is used by node_access to define
    // the language for which the checks should run.
    $query->addMetaData('langcode', $this->getLangCode());
    $query->accessCheck();
    return $query;
  }

}
