<?php

namespace Drupal\entity_repository\Repository;

use Drupal\comment\Entity\Comment;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Base class for comment related queries.
 */
class CommentRepository extends EntityRepository {

  /**
   * {@inheritdoc}
   */
  protected $entityType = 'comment';

  /**
   * {@inheritdoc}
   */
  protected function getBaseQuery(): QueryInterface {
    $query = $this->entityTypeManager->getStorage($this->entityType)
      ->getQuery();
    $query->condition('status', Comment::PUBLISHED);
    $query->condition('comment_type', $this->bundles, 'IN');
    $query->condition('langcode', $this->getLangCode());
    $query->addTag('comment_access');
    $query->accessCheck();
    return $query;
  }

  /**
   * Gets comments by the given commented entity.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $entity_id
   *   the entity ID.
   * @param string $field_name
   *   The field name.
   * @param int $pager
   *   The limit of the amount of items.
   * @param array $sort
   *   Array containing the sort logic.
   *
   * @return array
   *   Array containing the entities.
   */
  public function findByCommentedEntity(string $entity_type, int $entity_id, string $field_name, int $pager = NULL, array $sort = []): array {
    return $this->findBy([
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
      'field_name' => $field_name,
    ], $pager, $sort);
  }

  /**
   * Get the number of matching comments by the given criteria.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $entity_id
   *   the entity ID.
   * @param string $field_name
   *   The field name.
   *
   * @return int
   *   The nr of matching comments.
   */
  public function countByCommentedEntity(string $entity_type, int $entity_id, string $field_name) {
    return $this->countBy([
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
      'field_name' => $field_name,
    ]);
  }

}
