<?php

namespace Drupal\entity_repository\Repository;

use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Base class for taxonomy term related queries.
 */
class TaxonomyTermRepository extends EntityRepository {

  /**
   * {@inheritdoc}
   */
  protected $entityType = 'taxonomy_term';

  /**
   * The taxonomy vocabularies to check.
   *
   * @var array
   *   The taxonomy vocabularies to check.
   */
  protected array $vocabularies = [];

  /**
   * Get the vocabularies on which to filter.
   *
   * @return array
   *   The vocabularies on which to filter.
   */
  public function getVocabularies(): array {
    return $this->vocabularies;
  }

  /**
   * Set vocabularies.
   *
   * @param array $vocabularies
   *   The vocabularies on which to filter.
   *
   * @return TaxonomyTermRepository
   *   The current class.
   */
  public function setVocabularies(array $vocabularies): TaxonomyTermRepository {
    $this->vocabularies = $vocabularies;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseQuery(): QueryInterface {
    $query = $this->entityTypeManager->getStorage($this->entityType)
      ->getQuery();
    $query->condition('status', TRUE);
    $query->condition('vid', $this->getVocabularies(), 'IN');
    $query->condition('langcode', $this->getLangCode());
    $query->addTag('taxonomy_term_access');
    $query->accessCheck();
    return $query;
  }

}
