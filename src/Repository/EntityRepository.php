<?php

namespace Drupal\entity_repository\Repository;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Select;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Pager\PagerManagerInterface;

/**
 * Base repository class for any kind of entity.
 */
abstract class EntityRepository implements EntityRepositoryInterface {

  /**
   * The bundles.
   *
   * @var array
   *   The bundles
   */
  protected array $bundles;

  /**
   * The entity type.
   *
   * @var string
   *   The entity type.
   */
  protected $entityType;

  /**
   * The Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   *   The Database connection.
   */
  protected $connection;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   *   The language manager.
   */
  protected $languageManager;

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The Entity type manager.
   */
  protected $entityTypeManager;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * EventRepository constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The connection object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManager $languageManager
   *   The language manager.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pagerManager
   *   The pager manager.
   */
  public function __construct(
    Connection $connection,
    EntityTypeManagerInterface $entityTypeManager,
    LanguageManager $languageManager,
    PagerManagerInterface $pagerManager,
  ) {
    $this->connection = $connection;
    $this->languageManager = $languageManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->pagerManager = $pagerManager;
  }

  /**
   * A base query that is used as the starting point for follow up queries.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The base query.
   */
  abstract protected function getBaseQuery() : QueryInterface;

  /**
   * Return number of results of the base query.
   *
   * @return int
   *   The number of results.
   */
  public function countBaseQuery(): int {
    $query = $this->getBaseQuery();
    $count = $query->count()->execute();
    return is_array($count) ? 0 : (int) $count;
  }

  /**
   * {@inheritdoc}
   */
  public function findAll(int $pager = NULL, array $sort = []) : array {
    $query = $this->getBaseQuery();

    if ($pager) {
      $query->pager($pager);
    }

    $this->addEntityQuerySort($query, $sort);

    return $this->getResults($query);
  }

  /**
   * {@inheritdoc}
   */
  public function findBy(array $criteria, int $pager = NULL, array $sort = []): array {
    $query = $this->getBaseQuery();

    foreach ($criteria as $field => $value) {
      $query->condition($field, $value, is_array($value) ? 'IN' : '=');
    }

    if ($pager) {
      $query->pager($pager);
    }

    $this->addEntityQuerySort($query, $sort);

    return $this->getResults($query);
  }

  /**
   * {@inheritdoc}
   */
  public function countBy(array $criteria): int {
    $query = $this->getBaseQuery();

    foreach ($criteria as $field => $value) {
      $query->condition($field, $value);
    }

    $count = $query->count()->execute();
    return is_array($count) ? 0 : (int) $count;
  }

  /**
   * {@inheritdoc}
   */
  public function loadByIds(array $ids) : array {
    return $this->entityTypeManager->getStorage($this->entityType)->loadMultiple($ids);
  }

  /**
   * Helper function to get the results and load them as Node entities.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query object.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array containing entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getResults(QueryInterface $query) {
    $results = (array) $query->execute();

    if (empty($results)) {
      return [];
    }

    return $this->entityTypeManager
      ->getStorage($this->entityType)
      ->loadMultiple($results);
  }

  /**
   * Helper function to get the current language code.
   *
   * @return string
   *   The current language.
   */
  protected function getLangCode() {
    return $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
  }

  /**
   * Helper function to get the current date.
   *
   * @return string
   *   Get the current date.
   */
  protected function getCurrentDate(): string {
    return date('Y-m-dTh:i:s');
  }

  /**
   * Sets the bundles.
   *
   * @param array $bundles
   *   The bundles.
   */
  public function setBundles(array $bundles): void {
    $this->bundles = $bundles;
  }

  /**
   * Sets the entity type.
   *
   * @param string $entityType
   *   The entity type.
   */
  public function setEntityType($entityType): void {
    $this->entityType = $entityType;
  }

  /**
   * Initializes a pager on a query.
   *
   * @param \Drupal\Core\Database\Query\Select $query
   *   The select query.
   * @param int $limit
   *   The nr of results per page.
   *
   * @return \Drupal\Core\Database\Query\Select|null
   *   Select with initialized pager.
   */
  protected function initializeQueryPager(Select $query, int $limit = 12) : ?Select {
    $statement = $query->countQuery()->execute();

    if (!$statement instanceof StatementInterface) {
      return NULL;
    }

    $total = (int) $statement->fetchField();
    $pager = $this->pagerManager->createPager($total, $limit);
    $query->range($pager->getCurrentPage(), $limit);
    return $query;
  }

  /**
   * Add one or multiple sort elements to an entity query.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The select query.
   * @param array $sort
   *   Array containing sort logic. This variable can contain multiple sorts.
   */
  protected function addEntityQuerySort(QueryInterface $query, array $sort): void {
    if (!empty($sort)) {
      if (array_key_exists('field', $sort)) {
        $sort = [$sort];
      }

      foreach ($sort as $sortItem) {
        $query->sort($sortItem['field'], $sortItem['dir'], array_key_exists('langcode', $sortItem) ? $sortItem['langcode'] : $this->getLangCode());
      }
    }
  }

}
