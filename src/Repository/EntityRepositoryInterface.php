<?php

namespace Drupal\entity_repository\Repository;

/**
 * Interface for repository classes.
 */
interface EntityRepositoryInterface {

  /**
   * Sort ascending value.
   */
  public const SORT_ASCENDING = 'ASC';

  /**
   * Sort descending value.
   */
  public const SORT_DESCENDING = 'DESC';

  /**
   * Get All content by bundle.
   *
   * @param int $pager
   *   The limit of the amount of items.
   * @param array $sort
   *   Array containing the sort logic.
   *
   * @return array
   *   Array containing the entities.
   */
  public function findAll(int $pager = NULL, array $sort = []) : array;

  /**
   * Gets entities by criteria.
   *
   * @param array $criteria
   *   Array containing the criteria to which the entity should match.
   * @param int $pager
   *   The limit of the amount of items.
   * @param array $sort
   *   Array containing the sort logic.
   *
   * @return array
   *   Array containing the entities.
   */
  public function findBy(array $criteria, int $pager = NULL, array $sort = []) : array;

  /**
   * Gets nr of entities by criteria.
   *
   * @param array $criteria
   *   Array containing the criteria to which the entity should match.
   *
   * @return int
   *   The number of matching entities.
   */
  public function countBy(array $criteria): int;

  /**
   * Gets entities by ids.
   *
   * @param array $ids
   *   Array containing ids.
   *
   * @return mixed
   *   Entities matching the ids.
   */
  public function loadByIds(array $ids);

}
